using UnityEngine;
using System.Collections;

public class LauncherUI : MonoBehaviour {

	//Game[] gameList;

	public string[] filenames;
	public GameObject[] panes;
	public GameObject titleSpinner;
	public GameObject[] titles;
	public GameObject spinnerBacking;
	public GameObject camPivot;
	
	public GameObject arrowL;
	public GameObject arrowR;
	
	public int gameIndex;
	
	float timeOut = 0f;

	float rotAmount;


	void Start () {
		
		Screen.showCursor = false;
		
		
		Vector3 rot = titleSpinner.transform.eulerAngles;

		rotAmount = 360f/titles.Length;

		rot.y = gameIndex * rotAmount;

		titleSpinner.transform.eulerAngles = rot;
		
		for (int i=0; i<filenames.Length; ++i)
		{

			//set panes' positions
			if (i != gameIndex)
			{
				iTween.Init(panes[i]);
				Vector3 p = panes[i].transform.position;
				p.y = -15;
				panes[i].transform.position = p;
				//iTween.MoveTo(panes[i], iTween.Hash("y", -15, "time", 0.5, "easetype", iTween.EaseType.easeInOutQuart));
			}
			
			
			//look at the camera
			Vector3 r = titles[i].transform.eulerAngles;
			r.y = 180;
			titles[i].transform.eulerAngles = r;
			
		}
		
		
		
	}
	
	
	
	void Update () {
	
		if ((Input.GetButton("AnyLeft") && timeOut <= 0f) || (Input.GetButtonDown("AnyLeft") && timeOut <= 0.3f))
		{
			iTween.MoveTo(panes[gameIndex], iTween.Hash("y", -15, "time", 0.5, "easetype", iTween.EaseType.easeInOutQuart));
			gameIndex = ( gameIndex == 0 ) ? filenames.Length - 1 : gameIndex - 1;
			iTween.MoveTo(panes[gameIndex], iTween.Hash("y", -2.12, "time", 0.5, "easetype", iTween.EaseType.easeInOutQuart));
			
			iTween.RotateTo(titleSpinner, iTween.Hash("y", gameIndex * rotAmount, "time", 0.5, "easetype", iTween.EaseType.easeInOutQuart));
			
			iTween.ScaleFrom(arrowL, iTween.Hash("scale", Vector3.one * 0.18f, "time", 0.3, "easetype", iTween.EaseType.easeOutQuad));
			iTween.MoveFrom(arrowL, iTween.Hash("x", arrowL.transform.position.x - 0.3f, "time", 0.3, "easetype", iTween.EaseType.easeOutQuad));
			
			//iTween.RotateFrom(camPivot, iTween.Hash("y", -5, "time", 0.5, "easetype", iTween.EaseType.easeInOutQuart));
			
			//iTween.MoveFrom(spinnerBacking, iTween.Hash("x", spinnerBacking.transform.position.x + 0.7f, "time", 0.3, "easetype", iTween.EaseType.easeOutQuad));
			//iTween.ColorFrom(spinnerBacking, iTween.Hash("a", 0.1f, "time", 0.3, "easetype", iTween.EaseType.easeOutQuad));
			
			
			timeOut = 0.5f;
			
			
		}
		else if ((Input.GetButton("AnyRight") && timeOut <= 0f) || (Input.GetButtonDown("AnyRight") && timeOut <= 0.3f))
		{
			iTween.MoveTo(panes[gameIndex], iTween.Hash("y", -15, "time", 0.5, "easetype", iTween.EaseType.easeInOutQuart));
			gameIndex = (gameIndex + 1) % filenames.Length;
			iTween.MoveTo(panes[gameIndex], iTween.Hash("y", -2.12, "time", 0.5, "easetype", iTween.EaseType.easeInOutQuart));
			
			iTween.RotateTo(titleSpinner, iTween.Hash("y", gameIndex * rotAmount, "time", 0.5, "easetype", iTween.EaseType.easeInOutQuart));
			
			iTween.ScaleFrom(arrowR, iTween.Hash("scale", Vector3.one * 0.18f, "time", 0.3, "easetype", iTween.EaseType.easeOutQuad));
			iTween.MoveFrom(arrowR, iTween.Hash("x", arrowR.transform.position.x + 0.3f, "time", 0.3, "easetype", iTween.EaseType.easeOutQuad));
			
			//iTween.RotateFrom(camPivot, iTween.Hash("y", 5, "time", 0.5, "easetype", iTween.EaseType.easeInOutQuart));
			
			//iTween.ScaleFrom(spinnerBacking, iTween.Hash("x", 0.7f, "time", 0.3, "easetype", iTween.EaseType.easeOutQuad));
			//iTween.ColorFrom(spinnerBacking, iTween.Hash("a", 0.5f, "time", 0.3, "easetype", iTween.EaseType.easeOutQuad));
			
			
			timeOut = 0.5f;
		}
		
		if (Input.GetButtonDown("AnyA") || Input.GetButtonDown("AnyB") ||
			Input.GetButtonDown("AnyC") || Input.GetButtonDown("AnyD") ||
			Input.GetButtonDown("AnyStart"))
		{
			StartCoroutine(ProcessLauncher.Launch(filenames[gameIndex]));
			//TODO...
		}
		
		
		for (int i=0; i<filenames.Length; ++i)
		{
			Vector3 r = titles[i].transform.eulerAngles;
			r.y = 180;
			titles[i].transform.eulerAngles = r;
		}
		
		timeOut -= Time.deltaTime;
		
		
		//Debug.Log(filenames[gameIndex]);
		
		
		
	}
	
	
	
}