using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class StartTasks : MonoBehaviour {

	private const System.String launcherName = "royalLauncher";

	
	void Start () {
		RemoveBorderAndMoveToCorner();
	}
	
	[DllImport("user32.dll")]
	private static extern System.IntPtr FindWindow(string sClassName, string sAppName);
	
	[DllImport("user32.dll")]
	private static extern bool MoveWindow(System.IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);
	
	[DllImport("user32.dll")]
	private static extern long GetWindowLong(System.IntPtr hWnd, int nIndex);
	
	[DllImport("user32.dll")]
	private static extern long SetWindowLong(System.IntPtr hWnd, int nIndex, long dwNewLong);
	
	[DllImport("user32.dll")]
	private static extern bool SetWindowPos(System.IntPtr hWnd, System.IntPtr hWndInsertAfter, int x, int y, int cx, int cy, uint uFlags);
	
	private const int GWL_STYLE = -16;
	private const int WS_CAPTION = 0x00C00000;
	private const int WS_THICKFRAME = 0x00040000;
	private const int WS_MINIMIZE = 0x20000000;
	private const int WS_MAXIMIZE = 0x01000000;
	private const int WS_SYSMENU = 0x00080000;
	
	private const int GWL_EXSTYLE = -20;
	private const int WS_EX_DLGMODALFRAME = 0x00000001;
	private const int WS_EX_CLIENTEDGE = 0x00000200;
	private const int WS_EX_STATICEDGE = 0x00020000;
	
	private const int SWP_FRAMECHANGED = 0x0020;
	private const int SWP_NOMOVE = 0x0002;
	private const int SWP_NOSIZE  = 0x0001;
	private const int SWP_NOZORDER = 0x0004;
	private const int SWP_NOOWNERZORDER = 0x0200;
	

	private static void RemoveBorderAndMoveToCorner()
	{
		
		System.IntPtr hWnd = FindWindow(null, launcherName);
			
		if (hWnd.Equals(System.IntPtr.Zero))
		{
			UnityEngine.Debug.LogError("can't retrieve window handle");
			return;
		}	
		
		long lStyle = GetWindowLong(hWnd, GWL_STYLE);
		long lExStyle = GetWindowLong(hWnd, GWL_EXSTYLE);
		
		if (lStyle == 0 || lExStyle == 0)
		{
			UnityEngine.Debug.LogError("can't retrieve window styles");
			return;			
		}
		
		lStyle &= ~(WS_CAPTION | WS_THICKFRAME | WS_MINIMIZE | WS_MAXIMIZE | WS_SYSMENU);
		SetWindowLong(hWnd, GWL_STYLE, lStyle);
		
		lExStyle &= ~(WS_EX_DLGMODALFRAME | WS_EX_CLIENTEDGE | WS_EX_STATICEDGE);
		SetWindowLong(hWnd, GWL_EXSTYLE, lExStyle);
		
		
		SetWindowPos(hWnd, (System.IntPtr)null, 0,0,0,0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);
		
		MoveWindow(hWnd, 0, 0, 640, 480, false);
		
		
	}
	
	
}
