v4

Hi! This is the Arcade Royale launcher. Some notes:

- This is a Unity 3.5 project. Other versions of Unity probably won't like it very much.

- Since the cab's CRT runs at 640x480 and we don't want the launcher itself to run fullscreen (since that causes various unspeakable problems) we run at 640x480 windowed but do some winapi calls on launch to get rid of the window border and position the window in the right place. Seamless!

- For the latter to work, we need to know the window name, which is set in `Edit > Project Settings > Player > Product Name`. Since the script equivalent, `PlayerSettings.productName`, is an editor class, we can't access it at runtime, so we just keep this name in a constant in a script. All this means is that if you want to change the name from "royalLauncher", change the `launcherName` string in `StartTasks` to match.

- Included in this repo are builds of all games currently supported by the launcher (7 as of this writing.) Unzip these into the root of the Unity build directory (i.e. alongside `royalLauncher_Data`.) As of right now the folder name has to match the executable filename, which should reside immediately inside the game subdirectory (i.e. not buried in a subfolder of its own.) Game names are set in the inspector on the "Launcher UI" script attached to the main camera.

- Stuff that should probably be added/changed at some point:
	- Basic info
		- Game information: number of players (eg. 1-2, 2-4, 4, etc.)
		- Pre-launch (skippable) instruction screen for each game
		- Fade across multiple screenshots (and/or muted video clips of gameplay, maybe)
	
	- Bloat
		- Fancy introduction
		- Music! Should be easily mutable/not drive everyone mad.
		- Fancy screensaver/attract mode

	- Maybe support for other "platters" of game carousels (for future "waves" of games) accessible with up/down on any joystick
	
	- Make game entries list an array of structs grouping associated GameObjects together, rather than a bunch of disparate arrays, to make adding/removing games a bit simpler. Will this serialize in the inspector properly?



sa 7/2/2012